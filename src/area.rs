use std::fs::File;
use std::path::Path;
use std::io::{Result, BufReader, Read, Error, ErrorKind, Write};
use std::io::Cursor;
use std::string::ToString;
use std::iter::IntoIterator;
use byteorder::ReadBytesExt;
use encoding::{Encoding, DecoderTrap};
use encoding::all::WINDOWS_1250;
use pushback::PushbackReader;
use std::cmp;

struct AreaFile<R: Read> {
    r: R,
}

struct AreaIterator<R: Read> {
    af: AreaFile<R>,
    mem: Vec<u8>,
    lock: bool,
    key: Vec<u8>,
}

impl <R: Read> AreaFile<R> {
    fn new(r: R) -> AreaFile<R> {
        AreaFile { r: r }
    }
}

impl <R: Read> IntoIterator for AreaFile<R> {
    type Item = Vec<u8>;
    type IntoIter = AreaIterator<R>;

    fn into_iter(self) -> AreaIterator<R> {
        AreaIterator { 
            af: self,
            mem: Vec::new(),
            lock: false,
            key: [0x70, 0x6c, 0x65, 0x70, 0x6c, 0x6].to_vec(),
        }
    }
}

impl <R: Read> Iterator for AreaIterator<R> {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Vec<u8>> {
        if self.lock {
            // How to use FusedIterator?
            return None;
        }

        let len = match self.af.r.read_u8() {
            Ok(0xFA) => 0xFA,
            _ => return None
        };

        self.mem.resize(len, 0);
        let read = match self.af.r.read(&mut self.mem) {
            Ok(read) => {
                if read != len {
                    self.lock = true;
                }

                read
            }
            Err(_) => {
                self.lock = true;
                return None;
            }
        };

        let r = self.mem.as_slice()[0 .. read]
            .iter()
            .zip(self.key.iter() .cycle())
            .map(|(a, b)| *a ^ *b)
            .take_while(|c| *c != 0xFE);

        Some(r.collect())
    }
}

fn make_error<T: ToString>(msg: T) -> Result<()> {
    Err(Error::new(ErrorKind::Other, msg.to_string()))
}

pub fn decode_file(file_name: &str) {
    let input_file_path = Path::new(file_name);
    let mut output_file_path = input_file_path.to_path_buf();
    output_file_path.set_extension("txt");

    let input_file = input_file_path.to_str().unwrap();
    let output_file = output_file_path.to_str().unwrap();

    match decode_are(input_file, output_file) {
        Ok(_) => println!("Decoded {}", output_file),
        Err(msg) => println!("Error {}: {}", input_file, msg),
    }
}

fn decode_are(path: &str, out: &str) -> Result<()> {
    let area = AreaFile::new(File::open(path)?);

    for chunk in area {
        for binary_line in chunk.split(|c| *c == 0x01) {
            if binary_line.len() == 0 {
                continue;
            }

            let line = match WINDOWS_1250.decode(binary_line, DecoderTrap::Strict) {
                Ok(l) => l,
                Err(msg) => {
                    panic!("Encoding error! {:?}", msg);
                }
            };

            if line.chars().nth(0).unwrap() == '(' {
                println!();
            }

            println!("{}", line);
        }
    }

    Ok(())
}
