use argparse::{ArgumentParser, StoreTrue, Collect};

extern crate argparse;
extern crate byteorder;
extern crate encoding;

mod area;
mod pushback;

struct Args {
    decode_are: bool,
    file_names: Vec<String>,
}

impl Args {
    fn new() -> Args {
        Args {
            decode_are: false,
            file_names: Vec::new(),
        }
    }

    fn nothing_to_do(&self) -> bool { !self.decode_are }
}

fn parse_args(context: &mut Args) {
    let mut ap = ArgumentParser::new();
    ap.set_description("Decoder for data files of Otchłań");
    ap.refer(&mut context.decode_are).add_option(&["--are"], StoreTrue, "Decode .ARE file");
    ap.refer(&mut context.file_names).add_argument("files", Collect, "Filenames to process");
    ap.parse_args_or_exit();
}

fn main() {
    let mut ctx = Args::new();
    parse_args(&mut ctx);

    if ctx.nothing_to_do() {
        println!("Nothing to do! Use `-h` to get some help.");
        return;
    }

    if ctx.decode_are {
        ctx.file_names.iter().for_each(|ref f| area::decode_file(f))
    }
}
