use std::io::{Read, Result, Cursor};
use std::cmp;

const REALLOC_THRESHOLD: usize = 1024;

pub struct PushbackReader<R> {
    inner: R,
    buf: Vec<u8>,
    read_idx: usize,
}

impl <R> PushbackReader<R> {
    pub fn new(inner: R) -> PushbackReader<R> {
        PushbackReader {
            inner: inner,
            buf: Vec::new(),
            read_idx: 0,
        }
    }

    pub fn push_slice(&mut self, arr: &[u8]) {
        self.buf.extend_from_slice(arr);
        self.try_cleanup();
    }

    fn try_cleanup(&mut self) {
        if self.read_idx > REALLOC_THRESHOLD {
            let new_buf = {
                let (_, right) = self.buf.split_at(self.read_idx);
                right.to_vec()
            };

            self.buf = new_buf;
            self.read_idx = 0;
        }
    }

    fn buf_avail(&self) -> usize {
        self.buf.len() - self.read_idx
    }

    fn get_ref(&self) -> &R { &self.inner }
    fn get_mut(&mut self) -> &mut R { &mut self.inner }
    fn into_inner(self) -> R { self.inner }
}

impl<R: Read> Read for PushbackReader<R> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let pre_buf = &self.buf.as_slice()[self.read_idx ..];
        let n = cmp::min(pre_buf.len(), buf.len());

        self.inner.read(&mut buf[n ..])?;

        let mut write_idx = 0;
        for item in &pre_buf[0 .. n] {
            buf[write_idx] = *item;
            write_idx += 1;
        }

        self.read_idx += n;
        Ok(n + buf[n ..].len())
    }
}

